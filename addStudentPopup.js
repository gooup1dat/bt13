import { renderTable } from "./script.js";
import { dataStudent } from "./script.js";

// create a overlay, append to body and style it 
function createOverlay() {
    let overlay = document.createElement("div");
    overlay.id = "overlay";
    overlay.className = "fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-100";
    document.body.appendChild(overlay);
    return overlay;
}

// remove overlay when click outside the popup
document.addEventListener('click', function(e){
    if(e.target.id === 'overlay'){
        e.target.remove();
    }
});

// create a popup to add student
export function createAddStudentPopup(){
    let overlay = createOverlay();
    let popup = document.createElement("div");
    popup.id = "addStudentPopup";
    popup.className = "content w-[550px] p-6 border rounded-xl fixed top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 bg-white shadow-c-icon z-1000";
    // html content of popup
    popup.innerHTML =  `
            <form action="#" id="form">
                <div class="title flex flex-col p-6 ">
                    <h1 class="whitespace-nowrap font-semibold text-2xl tracking-tighter">Thêm sinh viên</h1>
                    <!-- <p class="text-sm text-gray-500">Fill out the form below.</p> -->
                </div>
                <div class="form-elements p-6 grid gap-6">
                    <div class="grid grid-cols-2 gap-4">
                        <div class="form-group flex flex-col gap-2">
                              <label class="text-sm font-medium" for="student-code">MSSV<span class="text-[10px] font-medium">(mã số sinh viên)</span></label>
                              <input type="student-code" class="form-control px-3 py-2 text-sm border leading-none h-10 w-full rounded-md" name="student-code" id="student-code" aria-describedby="helpId" placeholder="Nhập mã sinh viên" required>
                        </div>
                        <div class="form-group flex flex-col gap-2">
                          <label class="text-sm font-medium" for="name">Họ và tên</label>
                          <input type="text" class="form-control px-3 py-2 text-sm border leading-none h-10 w-full rounded-md" name="name" id="name" aria-describedby="helpId" placeholder="Nhập tên của bạn" required>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-4">
                        <div class="form-group flex flex-col gap-2">
                            <label class="text-sm font-medium" for="email">Email</label>
                            <input type="email" class="form-control px-3 py-2 text-sm border leading-none h-10 w-full rounded-md" name="email" id="email" aria-describedby="helpId" placeholder="Nhập email của bạn " required>
                          </div>

                        <div class="form-group flex flex-col gap-2">
                            <label class="text-sm font-medium" for="phone">Số điện thoại</label>
                            <input type="text" class="form-control px-3 py-2 text-sm border leading-none h-10 w-full rounded-md" name="phone" id="phone" pattern="^(0|\+84)(3|5|7|8|9)\d{8}$"  aria-describedby="helpId" placeholder="Nhập số điện thoại" required>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-4">
                        <div class="form-group flex flex-col gap-2">
                            <label class="text-sm font-medium" for="birthday">Ngày sinh</label>
                            <input type="date" name="birthday" class="px-3 py-2 text-sm border leading-none h-10 w-full rounded-md" id="birthday" value="2022-12-12" required>
                        </div>
                        <div class="form-group flex flex-col gap-2">
                            <label class="text-sm font-medium" for="">Giới tính</label>
                            <div class="gender-items flex gap-4 grow items-center">
                                <div class="gender-item">
                                    <input type="radio" name="gender" id="male" value="Nam" checked>
                                    <label class="text-sm font-medium" for="male">Nam</label>
                                </div>
                                <div class="gender-item">
                                    <input type="radio" name="gender" id="female" value="Nữ">
                                    <label class="text-sm font-medium" for="female">Nữ</label>
                                </div>
                                <div class="gender-item">
                                    <input type="radio" name="gender" id="other" value="Khác">
                                    <label class="text-sm font-medium" for="other">Khác</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="grid grid-cols-2 gap-4">
                        <div class="form-group flex flex-col gap-2">
                            <label class="text-sm font-medium" for="department">Khoa</label>
                            <select name="department" class="px-3 py-2 text-sm border leading-none h-10 w-full rounded-md bg-white" id="department">
                                <option value="000" disabled selected>Chọn khoa</option>
                                <option value="001">Công nghệ thông tin</option>
                                <option value="002">Cơ khí</option>
                                <option value="003">Điện tử viến thông</option>
                                <option value="004">Hóa</option>
                            </select>
                        </div>
                        <div class="form-group flex flex-col gap-2">
                            <label class="text-sm font-medium" for="clazz">Lớp sinh hoạt</label>
                            <input type="text" class="form-control px-3 py-2 text-sm border leading-none h-10 w-full rounded-md" name="clazz" id="clazz" aria-describedby="helpId" placeholder="Nhập tên lớp" required>
                        </div>
                    </div>
                    <input type="submit" value="Thêm" id="submit" class="p-3 bg-black text-white w-28 rounded-[15px] place-self-end hover:opacity-90 hover:cursor-pointer">
                </div>
            </form>
    `;

    overlay.appendChild(popup);

    // when submit form, get data and push to dataStudent, then render table
    document.getElementById("form").addEventListener("submit", function(e) {
        event.preventDefault();
        let student = getFormData(e);

        if(dataStudent.find(item => item.code === student.code)){
            alert("Mã sinh viên đã tồn tại");
            return;
        }

        dataStudent.push(student);

        // render table after add new student
        renderTable(dataStudent); 
        
        // remove popup after submit
        document.getElementById("overlay").remove();
    });
}


function getFormData(e) {
    // get data from form
    let formData = new FormData(e.target);
    let student = {
        code: formData.get("student-code"),
        name: formData.get("name"),
        email: formData.get("email"),
        phone: formData.get("phone"),
        dob: formData.get("birthday").split("-").reverse().join("/"),
        gender: formData.get("gender"),
        clazz: formData.get("clazz"),
    }
    
    let department = document.getElementById("department");
    student.department = department.options[department.selectedIndex].value;

    return student;
}