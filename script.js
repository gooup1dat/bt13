import { createAddStudentPopup } from "./addStudentPopup.js";

export var dataStudent = [
    {
        code: "001",
        name: "Nguyen Van A",
        email: "nva@gmail.com",
        phone: "0123456789",
        dob: "01/01/1990",
        gender: "Nam",
        department: "CNTT",
        clazz: "DHCNTT13A"
    },
    {
        code: "002",
        name: "Tran Thi B",
        email: "ttb@gmail.com",
        phone: "0987654321",
        dob: "02/02/1991",
        gender: "Nữ",
        department: "CNTT",
        clazz: "DHCNTT13B"
    },
    {
        code: "003",
        name: "Le Van C",
        email: "lvc@gmail.com",
        phone: "0123987456",
        dob: "03/03/1992",
        gender: "Nam",
        department: "CNTT",
        clazz: "DHCNTT13C"
    },
    {
        code: "004",
        name: "Pham Thi D",
        email: "ptd@gmail.com",
        phone: "0912345678",
        dob: "04/04/1993",
        gender: "Nữ",
        department: "CNTT",
        clazz: "DHCNTT13D"
    },
    {
        code: "005",
        name: "Hoang Van E",
        email: "hve@gmail.com",
        phone: "0987654321",
        dob: "05/05/1994",
        gender: "Nam",
        department: "CNTT",
        clazz: "DHCNTT13E"
    },
    {
        code: "006",
        name: "Nguyen Thi F",
        email: "ntf@gmail.com",
        phone: "0123456780",
        dob: "06/06/1995",
        gender: "Nữ",
        department: "CNTT",
        clazz: "DHCNTT13F"
    },
    {
        code: "007",
        name: "Tran Van G",
        email: "tvg@gmail.com",
        phone: "0987654320",
        dob: "07/07/1996",
        gender: "Nam",
        department: "CNTT",
        clazz: "DHCNTT13G"
    },
    {
        code: "008",
        name: "Le Thi H",
        email: "lth@gmail.com",
        phone: "0123456790",
        dob: "08/08/1997",
        gender: "Nữ",
        department: "CNTT",
        clazz: "DHCNTT13H"
    },
    {
        code: "009",
        name: "Pham Van I",
        email: "pvi@gmail.com",
        phone: "0912345789",
        dob: "09/09/1998",
        gender: "Nam",
        department: "CNTT",
        clazz: "DHCNTT13I"
    },
    {
        code: "010",
        name: "Hoang Thi J",
        email: "htj@gmail.com",
        phone: "0987654312",
        dob: "10/10/1999",
        gender: "Nữ",
        department: "CNTT",
        clazz: "DHCNTT13J"
    },
    {
        code: "011",
        name: "Nguyen Van K",
        email: "nvk@gmail.com",
        phone: "0123456809",
        dob: "11/11/2000",
        gender: "Nam",
        department: "CNTT",
        clazz: "DHCNTT13K"
    },
];

export function renderTable(data) {
    let table = document.getElementById("studentTable");
    let tbody = table.getElementsByTagName("tbody")[0];
    tbody.innerHTML = "";
    for (let row of data) {
        let trClass = "border-b hover:bg-alice-blue";
        let tdClass = "pt-4 pb-4 px-4 text-center font-normal font-helvetica text-gray-gray-700";
        let tr = document.createElement("tr");
        tr.className = trClass;
        for (let key in row) {
            let td = document.createElement("td");
            td.className = tdClass;
            td.textContent = row[key];
            tr.appendChild(td);
        }
        tbody.appendChild(tr);
    }
}

document.addEventListener("DOMContentLoaded", function() {
    renderTable(dataStudent);

    let addStudentBtn = document.getElementById("addStudent");
    addStudentBtn.removeEventListener("click", function() {});
    addStudentBtn.addEventListener("click", function() {
        createAddStudentPopup();
    });
});


